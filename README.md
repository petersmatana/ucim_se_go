# nastaveni Golang

**GOROOT**

???

**GOHOME**

_location of your workspace_

takze v mem pripade jedine repo s golangem

**GOPATH**

kde je v mym systemu nainstalovany go.

na ubuntu si to davam natvrdo na verzi 1.14 do adresare `/usr/share/go-1.14`

jetbrains jeste rozlisuji gopath na trech urovnich: globalni, projektovou a modulovou. takto se budou asi resit legacy veci k jednotlivym castem aplikace

**GOTOOLDIR**

???

# instalace Golang

## ubuntu

tenhle zdroj je problematickej nebo byla chyba mezi zidli a klavesnici.

1. pridam repo (root): `add-apt-repository ppa:longsleep/golang-backports`
2. update & install `apt update ; apt install golang-go`

## ubuntu 2

1. stahnout z URL: https://golang.org/dl/ zazipovany golang
2. ulozim nekam do Documents
3. na konec ~/.bashrc
   1. mkdir /home/peter/go
   2. export GOROOT=/home/peter/Documents/golang
   3. export GOPATH=$HOME/go
   4. export PATH=$GOPATH/bin:$GOROOT/bin:$PATH

GOROOT - ukazuje do adresare, kde je jazyk
GOPATH - ukazuje do adresare, kde jsou ulozene zavislosti
