package main

import "fmt"

// go nema tridy -> nema konstruktor
// takze si udelam neco cemu konstruktr
// budu rikat ktery pak spravny gentleman
// pouzije

type MyClass struct {
	text string
}

// prazdny konstruktr
func constructor() *MyClass {
	return &MyClass{}
}

// no tohle je nahouby, sak se hlavicka lisi? :(
// musim tomu dat uplne jiny nazev...
func constructor2(text string) *MyClass {
	myClass := MyClass{}
	myClass.text = text

	return &myClass
}

func main() {
	myClass := constructor2("LOL LOL")
	fmt.Printf("text v tride: %s", (*myClass).text)
}
