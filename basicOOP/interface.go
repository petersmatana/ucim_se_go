package main

import "fmt"

// interface funguje tak ze je pokud mam implementovanou
// metodu area() a perimeter() tak tyhle tridy automaticky
// implementujou interface shape

// schvalne u rectangle to porusuju a IDE mi nesviti

type shape interface {
	area() float64
	perimeter() float64
}

type square struct {
	name  string
	sideX float64
}

type rectangle struct {
	name         string
	sideX, sideY float64
}

func (s *square) area() float64 {
	return s.sideX * s.sideX
}

func (s *square) perimeter() float64 {
	return (*s).sideX * 4
}

func (r *rectangle) area() float64 {
	return r.sideX * r.sideY
}

// ale bohuzel netusim jak
// interface vyuzit

func main() {
	mySquare := square{}
	mySquare.sideX = 13.10
	fmt.Println("mySquare.area = ", mySquare.area())
}
