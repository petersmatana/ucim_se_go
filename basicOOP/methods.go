package main

import "fmt"

type Unce struct {
	volume float64
}

func (u Unce) getVolume() float64 {
	return float64(u.volume * 4.234)
}

func (u *Unce) setVolume(volume float64) {
	u.volume = volume
}

// tomuhle se rika: base type
type gallon float64

// (g gallon) se rika: receiver
func (g gallon) quart() float64 {
	return float64(g * 4)
}

func main() {
	gal := gallon(5)
	fmt.Printf("quart: %f\n", gal)

	u := Unce{volume: 2}
	fmt.Printf("volume unce: %f\n", u.getVolume())

	u.setVolume(3)
	fmt.Printf("volume unce: %f\n", u.getVolume())
}
