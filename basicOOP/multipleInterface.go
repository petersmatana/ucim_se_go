package main

type i1 interface {
	f1() string
}

type i2 interface {
	f2() int
}

// t1 BEGIN

type t1 struct {
}

func (t *t1) f1() string {
	return ""
}

func (t *t1) f2() int {
	return 0
}

// t1 END

// t2 BEGIN

type t2 struct {
}

func (t *t2) f1() string {
	return ""
}

func (t *t2) f2() int {
	return 0
}

// t2 END
