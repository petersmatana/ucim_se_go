package main

import "fmt"

type vehicle struct {
	make  string
	model string
}

type engine struct {
	fuel   fuel
	thrust int
}

type fuel int

// ----

type truck struct {
	vehicle
	engine
	load float64
}

func (t *truck) drive() {
	fmt.Printf("Truck: make: %s", t.vehicle.make)
}

type plane struct {
	vehicle
	engine
	seat int
}

func (p *plane) drive() {
	fmt.Printf("Plane drive")
}

func main() {
	truck := &truck{}
	truck.vehicle.make = "CAT"
	truck.vehicle.model = "krava"

	truck.drive()
}
