package singleton

import "fmt"

type Singleton interface {
	AddOne() int
}

type singleton struct {
	count int
}

func getInstance() Singleton {
	if instance == nil {
		fmt.Println("vytvarim novou instanci")

		// pouziju klicove slovo new
		// instance = new(singleton)

		// nebo to udelam rucne - potrebuju vyrobit
		// novou "instanci" struktury singleton
		instance = &singleton{count: 0}
	} else {
		fmt.Println("uz tu instanci mam")
	}

	return instance
}

func (s *singleton) AddOne() int {
	return 0
}

var instance *singleton
