package singleton

import "testing"

func TestGetInstance(t *testing.T) {
	instance1 := getInstance()

	if instance1 == nil {
		t.Error("expected pointer to Singleton")
	}

	instance2 := getInstance()

	if instance1 != instance2 {
		t.Error("two pointers are same")
	}

	//expectedCounter := counter1
}
