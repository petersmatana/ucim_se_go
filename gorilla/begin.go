package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)

type RequestMessage struct {
	Number int
}

type ResponseMessage struct {
	Text string `json:text`
}

func getHandler(writer http.ResponseWriter, request *http.Request) {
	responseMessage := &ResponseMessage{
		Text: "this is GET endpoint",
	}

	marshalData, error := json.Marshal(responseMessage)
	if error == nil {
		fmt.Println("there is some error in getHandler")
		return
	}

	writer.Header().Set("Content-Type", "application/json")
	writer.Write(marshalData)
}

func postHandler(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")

	var message ResponseMessage
	message = ResponseMessage{
		Text: "this is response message",
	}

	marshalData, error := json.Marshal(message)
	if error != nil {
		fmt.Println("there is some error in postHandler")
	}

	//var getMessage RequestMessage
	//error := json.Unmarshal(request., &getMessage)

	writer.Write(marshalData)
}

func main() {
	fmt.Println("starting app")
	router := mux.NewRouter()

	router.HandleFunc("/get", getHandler).Methods("GET")
	router.HandleFunc("/post", postHandler).Methods("POST")

	http.ListenAndServe("127.0.0.1:8080", router)

	fmt.Println("finishing app")
}
