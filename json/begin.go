package main

import (
	"encoding/json"
	"fmt"
)

type Message struct {
	Title string `json:"Title"`
	Body  string `json:"Body"`
	Id    int    `json:"Id"`
}

type MessageTransfer struct {
	Transfer [2]Message
}

func begin() {
	myMessage1 := Message{
		Title: "this is Title",
		Body:  "this is big Body",
		Id:    13,
	}

	myMessage2 := Message{
		Title: "title of message2",
		Body:  "body of message2",
		Id:    1,
	}

	var tmpTransfer [2]Message
	tmpTransfer[0] = myMessage1
	tmpTransfer[1] = myMessage2

	transfer := MessageTransfer{
		Transfer: tmpTransfer,
	}

	fmt.Println("origin message: ", transfer)

	marshalData, errorMarshal := json.Marshal(transfer)
	if errorMarshal != nil {
		fmt.Println("there is Marshal error: ", errorMarshal)
		return
	} else {
		fmt.Println("no Marshal error")
	}

	fmt.Println("marshal data: ", string(marshalData))

	var unmarshalData MessageTransfer
	errorUnmarshal := json.Unmarshal(marshalData, &unmarshalData)

	if errorUnmarshal != nil {
		fmt.Println("there is Unmarshal error: ", errorUnmarshal)
		return
	} else {
		fmt.Println("no Unmarshal error")
	}

	fmt.Println("unmarshal data: ", MessageTransfer(unmarshalData))
}

func jsonArray() {
	mojePole := []string{"prvek", "dalsi prvek"}
	output, _ := json.Marshal(mojePole)
	fmt.Println("serializace: ", output)
	fmt.Println("data: ", string(output))
}

func jsonHashMap() {
	hashMapa := make(map[string]int)
	hashMapa["k1"] = 10
	hashMapa["k2"] = 2

	output, _ := json.Marshal(hashMapa)
	fmt.Println("serializace: ", output)
	fmt.Println("data: ", string(output))
}

func main() {
	begin()
	//jsonArray()
	//jsonHashMap()
}
