package main

import (
	"fmt"
	"strconv"
)

func castingToString() {
	var text string
	text = "string " + strconv.Itoa(123)

	fmt.Println(text)
}

func begin() {
	// var deklaruje promennou

	var pointer *string
	var string1 string
	var string2 string

	string1 = "ahoj"
	string2 = "ahoj"

	pointer = &string1
	fmt.Printf("string1: %p \n", pointer)

	fmt.Printf("string: %s \n", *pointer)

	pointer = &string2
	fmt.Printf("string2: %p \n", pointer)

	string1 = "asd"
	pointer = &string1
	fmt.Printf("string1: %p \n", pointer)
}

func main() {
	castingToString()
}
