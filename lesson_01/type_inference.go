package main

import "fmt"

func type_inference() {
	// := je tzv.: short variable declaration
	// pry je to jen syntakticky cukr
	a := 13
	fmt.Println(a)

	b := "string"
	fmt.Println(b)

	// dal uz muzu delat jen normalni prirazeni
	a = 10
	b = "new string"
}

func var_declaration() {
	var a int
	var b string

	a = 13
	b = "string"

	fmt.Println(a)
	fmt.Println(b)
}

func main() {
	//type_inference()
	var_declaration()
}
