package main

import "fmt"

type Mesic string

func beginArray() {
	var mesice [12]Mesic

	fmt.Println(mesice)

	mesice[0] = "Leden"
	mesice[1] = "Únor"
	mesice[2] = "Březen"
	mesice[3] = "Duben"
	mesice[4] = "Květen"
	mesice[5] = "Červen"
	mesice[6] = "Červenec"
	mesice[7] = "Srpen"
	mesice[8] = "Září"
	mesice[9] = "Říjen"
	mesice[10] = "Listopad"
	mesice[11] = "Prosinec"

	fmt.Println(mesice)

	// proc nemuzu pouzit promennou mesice ?
	mesice2 := [12]Mesic{
		"Leden",
		"Únor",
		"Březen",
		"Duben",
		"Květen",
		"Červen",
		"Červenec",
		"Srpen",
		"Září",
		"Říjen",
		"Listopad",
		"Prosinec",
	}

	fmt.Println(mesice2)
}

func arrayIteration() {
	var pole [5]string
	pole[0] = "a"
	pole[1] = "b"
	pole[2] = "c"
	pole[3] = "d"
	pole[4] = "e"

	for i := 0; i < len(pole); i++ {
		fmt.Println(pole[i])
	}
}

func arrayCopy() {
	var pole [5]string
	pole[0] = "a"
	pole[1] = "b"
	pole[2] = "c"
	pole[3] = "d"
	pole[4] = "e"

	var pole2 [5]string
	// hluboka kopie!
	pole2 = pole

	fmt.Printf("pole: %v \n", pole)
	fmt.Printf("pole2: %v \n", pole2)

	pole2[3] = "zmena"

	fmt.Printf("pole: %v \n", pole)
	fmt.Printf("pole2: %v \n", pole2)
}

// tohle se neprelozi
//func arrayOfBytes() {
//	var pole []byte
//	pole = make([]byte, 'o', 'k')
//
//	fmt.Println(pole)
//}

func addIntoSlice() {
	//var pole []int
	pole := make([]int, 20)
	pole[1] = 123
	pole[4] = 2

	fmt.Println(pole)
}

func main() {
	//beginArray()
	//arrayIteration()
	//arrayCopy()
	//arrayOfBytes()
	addIntoSlice()
}
