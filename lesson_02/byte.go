package main

import "fmt"

func bajt() {
	var bajt byte

	bajt = 0x23
	fmt.Println(bajt)

	bajt = 13
	fmt.Println(bajt)

	bajt = 'a'
	fmt.Println(bajt)

	// tohle spadne
	//bajt = '⌘'
	//fmt.Println(bajt)
}

//func poleBajtu() {
//	var poleBajtu [2]byte
//
//
//}

func main() {
	//bajt()
}
