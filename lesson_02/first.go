package main

import "fmt"

type Id int
type Name string

func printData(id Id, name Name) {
	fmt.Printf("formated output %s %d", name, id)
}

func main() {
	fmt.Println("Hello world Smonty!")

	var name Name
	var id Id

	name = "smonty"
	id = 13

	printData(id, name)
}
