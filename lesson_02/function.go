package main

import (
	"fmt"
)

func withoutReturnValue() {
	var a int
	a = 10

	if a > 10 {
		fmt.Println("...")
	} else {
		return
	}
}

func returnString() string {
	return "hello world"
}

func returnFourInt() (int, int, int, int) {
	return 1, 2, 3, 4
}

func main() {
	//var x types.Void
	//x = withoutReturnValue()

	var result string
	result = returnString()
	fmt.Println(result)

	var a, b, c, d int
	a, b, c, d = returnFourInt()
	fmt.Printf("a: %d | b: %d | c: %d | d: %d |", a, b, c, d)
}
