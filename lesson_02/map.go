package main

import "fmt"

type Entita struct {
	id   int
	text string
}

type Key struct {
	id   int
	hash string
}

func content(exist bool, value string) {
	if exist {
		fmt.Println(value)
	} else {
		fmt.Println("nic tu neni")
	}
}

func basic() {
	var myMap map[int]string
	myMap = make(map[int]string)

	fmt.Println("prazdna mapa")
	fmt.Println(myMap)

	myMap[0] = "nulty prvek"
	myMap[1] = "prvni prvek"

	fmt.Println("naplnena mapa")
	fmt.Println(myMap)

	// jaky je rozdil mezi := a = ? :D
	value, exist := myMap[1]
	content(exist, value)

	value, exist = myMap[100]
	content(exist, value)
}

func contentAdvance(exist bool, value Entita) {
	if exist {
		fmt.Println(value)
	} else {
		fmt.Println("nic tu neni")
	}
}

func advance() {
	var myMap map[Key]Entita
	myMap = make(map[Key]Entita)

	fmt.Println("prazdna entita")
	fmt.Println(myMap)

	var key1 = Key{
		id:   1,
		hash: "adasdasd",
	}

	myMap[key1] = Entita{
		id:   1,
		text: "toto je text",
	}

	fmt.Println(myMap)

	var key Key
	var value Entita
	var exist bool

	key = Key{
		id:   1,
		hash: "toto je blbost",
	}

	value, exist = myMap[key]
	contentAdvance(exist, value)

	value, exist = myMap[key1]
	contentAdvance(exist, value)
}

func main() {
	//basic()
	advance()
}
