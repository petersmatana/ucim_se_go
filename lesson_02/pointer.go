package main

import "fmt"

type MyStruct struct {
	id   int
	text string
}

func begin() {
	var i int

	// pInt je ukazatel na typ int
	var pInt *int

	i = 13

	fmt.Printf("p_int = %d \n", pInt)
	fmt.Printf("&p_int = %p \n", &pInt)

	fmt.Printf("i = %d \n", i)

	// pInt ukazuje na hodnotu i v pameti, tedy 13
	pInt = &i

	fmt.Printf("p_int = %d \n", pInt)
	fmt.Printf("&p_int = %p \n", &pInt)
	fmt.Printf("*p_int = %d \n", *pInt)

	fmt.Printf("&i = %p \n", &i)

	// na adresu pInt ulozim cislo 10
	*pInt = 10

	fmt.Printf("p_int = %d \n", pInt)
	fmt.Printf("&p_int = %p \n", &pInt)
	fmt.Printf("*p_int = %d \n", *pInt)
}

func pointerOnStruct() {
	var struktura MyStruct

	var pId *int
	var pText *string

	pId = &struktura.id
	*pId = 1

	pText = &struktura.text
	*pText = "nejaky text"

	fmt.Printf("struktura %v \n", struktura)
	fmt.Printf("struktura %+v \n", struktura)
}

func pointerOnArray() {
	var pole [2]string

	pole[0] = "prvni prvek"
	pole[1] = "druhy prvek"

	var bunka *string

	bunka = &pole[1]
	*bunka = "ted jsem zmenil text"

	fmt.Printf("bunka = %#v \n", bunka)
	fmt.Printf("&bunka = %v \n", &bunka)
	fmt.Printf("*bunka = %v \n", *bunka)
}

func main() {
	begin()
	//pointerOnStruct()
	//pointerOnArray()
}
