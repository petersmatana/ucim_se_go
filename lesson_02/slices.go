package main

import "fmt"

func beginSlice() {
	var pole [5]string
	pole[0] = "a"
	pole[1] = "b"
	pole[2] = "c"
	pole[3] = "d"
	pole[4] = "e"

	fmt.Println(pole[1:3])

	// nefunguje to jako v pythonu
	// fmt.Println(pole[2:-3])

	// slice do konce pole
	fmt.Println(pole[1:])

	// slize ze slice
	fmt.Println(pole[1:][2:3])
}

func sliceModification() {
	var pole [5]string
	pole[0] = "a"
	pole[1] = "b"
	pole[2] = "c"
	pole[3] = "d"
	pole[4] = "e"

	slice := pole[:]

	// pokud modifikuju rez, modifikuju i pole
	for i := 0; i < len(slice); i++ {
		fmt.Printf("slice in %d: %s \n", i, slice[i])
		slice[i] = "value"
	}

	fmt.Printf("pole: %v \n", pole)
	fmt.Printf("slice: %v \n", slice)
}

func main() {
	//beginSlice()
	sliceModification()
}
