package main

import "fmt"

type Id int
type Name string
type Surname string

type User struct {
	id      Id
	name    Name
	surname Surname
}

func main() {
	var user1 User

	fmt.Println(user1)

	user1.id = 1
	user1.name = "pepa"
	user1.surname = "pepovic"

	fmt.Println(user1)

	var users [10]User
	users[4] = User{
		id:      1,
		name:    "name",
		surname: "surname",
	}

	userX := User{
		id:      2,
		name:    "user X",
		surname: "secret",
	}

	users[0] = userX

	fmt.Println(users)
}
