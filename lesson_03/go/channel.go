package main

import (
	"fmt"
)

func channelMessage(message string, channel chan int) {
	fmt.Printf("message: %s \n", message)
	channel <- 1
}

func example1() {
	// inicializuju channel
	channel := make(chan int)

	fmt.Println("begin")

	// asynchronne pustim metodu
	go channelMessage("text", channel)

	// tady muzu teoreticky channel uzavrit ale pak z nej nic nedostanu
	//close(channel)

	// dostanu data z kanalu
	code, status := <-channel
	fmt.Printf("received code: %d, status: %t \n", code, status)

	//close(channel)

	fmt.Println("end")
}

func deadLock() {
	channel := make(chan int)

	fmt.Println("begin")

	go channelMessage("text", channel)
	go channelMessage("text2", channel)
	go channelMessage("text3", channel)

	code, status := <-channel

	// donekonecna koukam jestli v kanalu nic neni
	for status {
		fmt.Printf("received code: %d, status: %t \n", code, status)

		// ale stejne to deadlockuje
		if !status {
			close(channel)
		}

		code, status = <-channel
	}

	fmt.Println("end")
}

func example3() {
	// inicializuju channel
	channel := make(chan int)

	fmt.Println("begin")

	// asynchronne pustim metodu
	go channelMessage("text", channel)
	go channelMessage("text2", channel)
	go channelMessage("text3", channel)

	// tady muzu teoreticky channel uzavrit ale pak z nej nic nedostanu
	//close(channel)

	// dostanu data z kanalu
	code, status := <-channel
	fmt.Printf("received code: %d, status: %t \n", code, status)

	code, status = <-channel
	fmt.Printf("received code: %d, status: %t \n", code, status)

	code, status = <-channel
	fmt.Printf("received code: %d, status: %t \n", code, status)

	//close(channel)

	fmt.Println("end")
}

func example4() {
	// inicializuju channel
	channel := make(chan int)

	fmt.Println("begin")

	// jedu sekvencne
	go channelMessage("text", channel)
	code, status := <-channel
	fmt.Printf("received code: %d, status: %t \n", code, status)

	go channelMessage("text2", channel)
	code, status = <-channel
	fmt.Printf("received code: %d, status: %t \n", code, status)

	go channelMessage("text3", channel)
	code, status = <-channel
	fmt.Printf("received code: %d, status: %t \n", code, status)

	fmt.Println("end")
}

func main() {
	//example1()
	//deadLock()
	//example3()
}
