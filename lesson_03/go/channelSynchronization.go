package main

import (
	"fmt"
)

func simpleWorker(done chan string, id int) {
	fmt.Printf("id workeru: %d\n", id)
	done <- "nejaky string"
}

func main() {
	done := make(chan string, 1)

	go simpleWorker(done, 10)

	value, status := <-done

	if status {
		fmt.Printf("result %s \n", value)
	} else {
		fmt.Printf("problem \n")
	}
}
