package main

import "fmt"

func worker(taskChannel chan int, workerDone chan bool) {
	for {
		value, status := <-taskChannel
		if status {
			fmt.Printf("worker %d receive data \n", value)
		} else {
			fmt.Printf("worker %d finish work \n", value)
			workerDone <- true
			return
		}
	}
}

func main() {
	taskChannel := make(chan int)
	workerDone := make(chan bool)

	go worker(taskChannel, workerDone)

	for i := 0; i < 20; i++ {
		fmt.Printf("sending task to worker: %d \n", i)
		taskChannel <- i
	}

	//close(taskChannel)

	value, status := <-workerDone

	if status {
		fmt.Print("\nfinish")
		fmt.Printf("\nvalue: %t \nstatus: %t \n", value, status)
	} else {
		fmt.Println("neco se rozbilo")
	}
}
