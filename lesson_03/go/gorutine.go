package main

import (
	"fmt"
	"time"
)

func message(message string, id int) {
	fmt.Printf("%d message: %s \n", id, message)
}

func main() {
	for i := 0; i < 100; i++ {
		go message("hello", i)
	}

	time.Sleep(5)

	fmt.Println("konec")
}
