package main

import (
	"fmt"
	"time"
)

func cons(toConsume <-chan int) {
	for {
		fmt.Printf("zkonzumovano %d\n", <-toConsume)
	}
}

func prod(toWriteChan chan<- int, number int) {
	for {
		toWriteChan <- number
		fmt.Printf("zapsan: %d\n", number)
	}
}

func middleware(check chan int, counter chan int) {
	for {
		counter <- (<-counter) + 1

		if (<-check) > 4 {
			value := <-check

			fmt.Printf("middleware funguje %d\n", value)

			check <- value * value
		}
	}
}

func sleep() {
	time.Sleep(100 * time.Microsecond)
}

func main() {
	middlewareCounter := make(chan int)

	store := make(chan int)

	for i := 0; i < 3; i++ {
		go prod(store, i)
		go middleware(store, middlewareCounter)
		go cons(store)

		sleep()

		// index += 1
	}

	fmt.Printf("middleware pouzit: %d", <-middlewareCounter)

	// sleep()

	// fmt.Println(<-store)
}
