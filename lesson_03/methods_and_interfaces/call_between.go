package main

import "fmt"

type MujInterface interface {
	fnc1()
}

type Struktura struct {
	nejakaHlaska  string
	nejakaHlaska2 string
}

func (s Struktura) fnc1() {
	fmt.Println("fnc1, nejakaHlaska: ", s.nejakaHlaska)
}

func main() {
	s := &Struktura{
		nejakaHlaska:  "hlaska 1",
		nejakaHlaska2: "hlaska 2",
	}

	s.fnc1()
}
