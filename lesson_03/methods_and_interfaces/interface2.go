package main

import "fmt"

type MyInterface interface {
	lengthF() int
	//dalsiHeft() string
}

type MyStruct struct {
	number int
	//nejakyText string
}

type Lol struct {
}

func lengthF(int MyInterface) int {
	//fmt.Printf("interface method: ")
	return int.lengthF()
}

//func dalsiHeft(int MyInterface) string {
//	return int.dalsiHeft()
//}

func (l Lol) lengthF() int {
	return 123
}

func main() {
	mojeStruktura := MyStruct{number: 10}
	fmt.Printf("bez interface: %d", lengthF(mojeStruktura))
}
