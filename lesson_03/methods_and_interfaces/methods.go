package main

import "fmt"

type MyLine struct {
	lineLength float64
	lineType   string
}

func (line MyLine) getLength() float64 {
	return line.lineLength
}

func (line *MyLine) setLength(length float64) {
	fmt.Printf("co mi prislo %f\n", length)
	// muzu to delat takto, to by me napadlo
	(*line).lineLength = length

	// tohle uplne nechapu jak je mozny
	// todo dostudovat
	line.lineLength = length
	fmt.Printf("co nastavuju line.lineLength %f\n", (*line).lineLength)
}

func main() {
	myLine := MyLine{lineLength: 42}
	fmt.Printf("delka line: %f\n", myLine.getLength())

	// takto atribut asi nenastavim
	myLine.setLength(123.0)
	fmt.Printf("delka line: %f\n", myLine.getLength())
}
