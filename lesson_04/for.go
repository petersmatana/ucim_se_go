package main

import "fmt"

func infinitLoop() {
	index := 0

	// atributy jsou nepovinne, stejne jako kdybych napsal:
	// for { ... }
	for {
		index += 1
		if index > 1000 {
			break
		}
		fmt.Printf("nekonecna smycka, index: %d\n", index)
	}
}

func infinitLoop2() {
	index := 0

	// ekvivalent while (podminka) { ... }
	for index < 10 {
		index += 1
		fmt.Printf("nekonecna smycka, index: %d\n", index)
	}

	index = 0

	for index < 10 {
		index += 1
		fmt.Printf("nekonecna smycka, index: %d\n", index)
	}
}

func iteracePole() {
	a := [...]int{100, 22, 3, 42, 5, 6}

	for index, item := range a {
		fmt.Printf("index: %d\n", index)
		fmt.Printf("item: %d\n\n", item)
	}

	s := "Hello world ěščř Σ"
	for index, character := range s {
		// fmt.Println("index: ", index, " character: ", character)
		fmt.Printf("index: %d, character: %c\n", index, character)
	}
}

func iteracePoleLepsi() {
	// protoze GO nedovoluje deklarovat promennou ktera se nevyuzije
	// a pokud nehodlam pouzivat promennou index tak ji mohu nahradit za
	// znak: _

	a := [...]int{100, 22, 3, 42, 5, 6}

	for _, item := range a {
		//fmt.Printf("index: %d\n", index)
		fmt.Printf("item: %d\n\n", item)
	}

	s := "Hello world ěščř Σ"
	for _, character := range s {
		//fmt.Printf("index: %d, character: %c\n", index)
		fmt.Printf("character: %c\n", character)
	}
}

func continuePriklad() {
	for i := 0; i < 100; i++ {
		if i%2 == 0 {
			continue
		} else {
			fmt.Printf("vypisuju %d\n", i)
		}
	}
}

func main() {
	//infinitLoop()
	//infinitLoop2()
	//iteracePole()
	//iteracePoleLepsi()
	continuePriklad()
}
