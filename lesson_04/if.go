package main

import "fmt"

func dlouhyIf(number int) {
	if number > 0 && number < 10 {
		fmt.Println("cislo > 0 a < 10")
	} else if number > 10 && number < 100 {
		fmt.Println("cislo > 10 a < 100")
	} else {
		fmt.Println("jinak")
	}
}

func getNumber() int {
	return 100
}

func vyhodnoceniVIfu() {
	if value := getNumber(); value < 50 {
		fmt.Println("value < 50")
	} else {
		fmt.Println("jinak")
	}
}

func zajimavost() {
	// ?
	// uplne nepobiram tu syntax
	// if _, ok := c.helpers[frame.Function]; !ok {
}

func main() {
	//dlouhyIf(40)
	//vyhodnoceniVIfu()
}
