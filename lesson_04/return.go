package main

import "fmt"

func test(number int) {
	if number > 10 {
		fmt.Println("number je vetsi")
		return
	}

	fmt.Println("nic")
	return
}

func divny() {
	// toto se zkompiluje a projde

	fmt.Println("neco delam")
	return
	fmt.Println("chtel bych delat neco dalsiho")
}

func vracimCislo() int {
	// tohle neprojde protoze prekladac testuje
	// return ve vsech nedosazitelnych blocich kodu

	fmt.Println("neco delam")
	return 10
	fmt.Println("chci delat neco dalsiho")

	// s timhle to uz bude fungovat
	return 1
}

func main() {
	//test(3)
	//divny()
	vracimCislo()
}
