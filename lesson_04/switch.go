package main

import (
	"fmt"
)

func switchBezNiceho() {
	switch {

	}
}

func switchDefault() {
	switch {
	default:
		fmt.Println("default")
	}
}

func switchVychozi() {
	vyrok := false
	switch vyrok {
	case true:
		fmt.Println("dalsi kod")
		switchBezNiceho()
		fmt.Println("true")
	case false:
		fmt.Println("dalsi kod")
		switchBezNiceho()
		fmt.Println("false")
	}
}

func switchPokrocily() {
	number := 13
	switch {
	case number > 10:
		fmt.Println("> 10")
	case number > 50:
		fmt.Println("> 50")
	default:
		fmt.Println("jinak")
	}
}

func switchPorovnani() {
	number := 4
	switch number {

	// takto to nejde

	//case number > 10:
	//	fmt.Println("> 10")
	//case number > 100:
	//	fmt.Println("> 100")

	case 1, 2, 3:
		fmt.Println("1 || 2 || 3")
	case 4, 5, 6:
		fmt.Println("4 || 5 || 6")
	default:
		fmt.Println("jinak")
	}
}

func switchFallThrough(number int) {
	// kdyz i v case 9 a 8 uvedu fallthrough
	// tak to padne do defaultniho stavu

	fmt.Printf("BEGIN FCE switchFallThrough, number: %d\n", number)

	switch number {

	case 0:
		fmt.Println("\tcase 0")

	case 1:
		fmt.Println("\tcase 1")
		fallthrough
	case 3:
		fmt.Println("\tcase 3")
		fallthrough
	case 5:
		fmt.Println("\tcase 5")
		fallthrough
	case 7:
		fmt.Println("\tcase 7")
		fallthrough
	case 9:
		fmt.Println("\tcase 9")
		fmt.Println("JE LICHE")
		fallthrough

	case 2:
		fmt.Println("\tcase 2")
		fallthrough
	case 4:
		fmt.Println("\tcase 4")
		fallthrough
	case 6:
		fmt.Println("\tcase 6")
		fallthrough
	case 8:
		fmt.Println("\tcase 8")
		fmt.Println("JE SUDE")
		fallthrough

	default:
		fmt.Println("\tdefaultni stav")
	}

	fmt.Println("END FCE switchFallThrough")
}

func provolavaniFallThrough() {
	for i := 0; i < 10; i++ {
		switchFallThrough(i)
	}
}

func main() {
	//dlouhyIf(40)
	//vyhodnoceniVIfu()
	//switchBezNiceho()
	//switchDefault()
	//switchVychozi()
	//switchPokrocily()
	//switchPorovnani()
	provolavaniFallThrough()
}
