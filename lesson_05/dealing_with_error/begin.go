package main

import (
	"errors"
	"fmt"
)

func ThisFuncReturnsError() (result string, err error) {
	return "", errors.New("this is error")
}

func wrap() (err error) {
	result, error := ThisFuncReturnsError()
	if error != nil {
		return errors.New("wrap func return error: " + error.Error())
	} else {
		fmt.Println("result func: ", result)
	}

	return nil
}

func main() {
	err := wrap()
	if err != nil {
		fmt.Println(err)
	}
}
