package defer_keyword

import "fmt"

func function(i int) {
	fmt.Printf("Defer %2d\n", i)
}

func goon(x int) {
	fmt.Printf("Current x value = %2d\n", x)
	defer function(x)

	x++

	fmt.Printf("Current x value = %2d\n", x)
	defer function(x)

	x++

	fmt.Printf("Current x value = %2d\n", x)
	defer function(x)

	x++

	fmt.Printf("Current x value = %2d\n", x)
	defer function(x)

	x++

	fmt.Printf("Current x value = %2d\n", x)
	defer function(x)
}

func main() {
	fmt.Println("program start")
	defer (func() {
		fmt.Println("program finished")
	})()

	x := 0

	for i := 0; i < 10; i++ {
		goon(x)
		x++
	}
}
