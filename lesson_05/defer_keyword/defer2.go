package defer_keyword

import "fmt"

func f1() {
	fmt.Println("f1")
}

func f2() {
	fmt.Println("f2")
}

func f3() {
	fmt.Println("f3")
}

func mainProgram() {
	fmt.Println("begin main program")
	defer f1()
	defer f2()
	defer f3()
	fmt.Println("end main program")
}

func stackingDefer() {
	fmt.Println("counting")

	for i := 0; i < 10; i++ {
		fmt.Printf("bez defer: %d\n", i)
		defer fmt.Printf("s defer: %d\n", i)
	}

	fmt.Println("done")
}

func main() {
	//fmt.Println("begin main")
	//mainProgram()
	//fmt.Println("end main")

	stackingDefer()
}
