package defer_keyword

import "fmt"

// resim jak se presne vyhodnocuji argumenty

func deferFunc(i int) {
	fmt.Println("Defer: ", i)
}

func classicFunc(i int) {
	fmt.Println("Classic: ", i)
}

func variable() {
	x := 0

	classicFunc(x)
	defer deferFunc(x)

	x += 1

	classicFunc(x)
	defer deferFunc(x)

	x += 1

	classicFunc(x)
	defer deferFunc(x)
}

func deferShowArray(array []int) {
	fmt.Println("deferShowArray: ", array)
}

func classicShowArray(array []int) {
	fmt.Println("classicShowArray: ", array)
}

func array() {
	// fce vidi az posledni znamou operaci nad polem
	// nejakou roli v tom hrajou ukazatele ale momentalne
	// nevim jakou...
	pole := []int{1, 2, 4}

	classicShowArray(pole)
	defer deferShowArray(pole)

	pole[1] = 100
	classicShowArray(pole)
	defer deferShowArray(pole)

	pole[1] = 0
	classicShowArray(pole)
	defer deferShowArray(pole)

	pole[2] = 13
	classicShowArray(pole)
	defer deferShowArray(pole)
}

func main() {
	//variable()
	array()
}
