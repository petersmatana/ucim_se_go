package defer_keyword

import (
	"fmt"
	"os"
)

func closeFile(fileName *os.File) (close bool, errorResult error) {
	errorResult = fileName.Close()

	if errorResult != nil {
		fmt.Println("error with closing file")
		return false, errorResult
	} else {
		fmt.Println("close file: ", fileName.Name())
		return true, errorResult
	}
}

func openFile(fileName string) (fileResult os.File, errorResult error) {
	file, error := os.Open(fileName)

	if error != nil {
		fmt.Println("can't open file: ", fileName)
		return *file, error
	} else {
		fmt.Println("opening file: ", fileName)
		return *file, error
	}
}

func testFileExistence(fileName string) (exist bool, errorResult error) {
	if _, error := os.Stat(fileName); os.IsNotExist(error) {
		fmt.Println("file is't exist: ", fileName)
		return false, error
	} else {
		fmt.Println("file exist: ", fileName)
		return true, error
	}
}

func program(sourceFilePath string) {
	defer fmt.Printf("\n\n")

	sourceFile := os.File{}
	var errorOpenFile error

	if exist, _ := testFileExistence(sourceFilePath); exist {
		sourceFile, errorOpenFile = openFile(sourceFilePath)
		defer closeFile(&sourceFile)

		if errorOpenFile == nil {
			fmt.Println("zapisuju")
			sourceFile.Write([]byte{115, 111, 109, 101, 10})
		} else {
			fmt.Println(errorOpenFile)
		}
	}
}

func main() {
	sourceFilePath1 := "/home/peter/go/src/awesomeProject/lesson_05/file"
	program(sourceFilePath1)

	sourceFilePath2 := "/home/peter/go/src/awesomeProject/lesson_05/file2"
	program(sourceFilePath2)

	sourceFilePath3 := "/dev/null"
	program(sourceFilePath3)

	sourceFilePath4 := "/non/existing/file"
	program(sourceFilePath4)
}
