package defer_keyword

import "fmt"

func fn1() (i int) {
	i = 1
	return i
}

func fn2() (i int) {
	defer func() {
		i = 2
	}()
	return i
}

func fn3() (i int) {
	defer func() {
		i += 2
	}()

	// todo
	// musim si dostudovat jaky je rozdil kdyz tu
	// mam: return 1
	// a nebo: return i
	// protoze kazda vec dela neco uplne jineho
	return 1
}

func main() {
	fmt.Println("navratova hodnota fce fn1: ", fn1())
	fmt.Println("navratova hodnota fce fn2: ", fn2())
	fmt.Println("navratova hodnota fce fn3: ", fn3())
}
