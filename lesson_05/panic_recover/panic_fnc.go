package main

import "fmt"

type CalculatorInterface interface {
	showError(err error, errorMessage string)
	divide(x, y float64) (result float64)
	calculation(x, y float64, operation string) (result float64)
}

type Calculator struct {
	zeroDivideException            string
	notSupportedOperationException string
}

func (c *Calculator) showError(errorMessage string) {
	if rec := recover(); rec != nil {
		fmt.Println("error message: ", errorMessage)
	}
}

func (c *Calculator) divide(x, y float64) (result float64) {
	defer c.showError(c.zeroDivideException)

	if y == 0.0 {
		panic(c.zeroDivideException)
	}

	return x / y
}

func (c *Calculator) calculation(x, y float64, operation string) (result float64) {
	defer c.showError(c.notSupportedOperationException)

	switch operation {
	case "divide":
		return c.divide(x, y)
	default:
		panic(c.notSupportedOperationException)
	}
}

func main() {
	var result float64

	c := &Calculator{
		zeroDivideException:            "can't divide by zero",
		notSupportedOperationException: "this operation is not supported",
	}

	result = c.calculation(1, 0, "divide")
	fmt.Println("result: ", result)

	result = c.calculation(1, 4, "divide")
	fmt.Println("result: ", result)

	result = c.calculation(1, 3, "addition")
	fmt.Println("result: ", result)
}
