package main

import (
	"errors"
	"fmt"
)

const DIVIDE = 1
const SUBTRACTION = 2

type Calculator2 interface {
	calculation2(x, y float64, operation int) (result float64, err error)
}

type divideZeroError struct {
	error string
}

func divide2(x, y float64) (result float64, err error) {
	if y == 0.0 {
		error := divideZeroError{error: "can't divide by zero"}
		return 0, errors.New(error.error)
	}

	return x / y, nil
}

func calculate2(x, y float64, operation int) (result float64, err error) {
	switch operation {
	case DIVIDE:
		return divide2(x, y)
	case SUBTRACTION:
		return 0, errors.New("this operation is not implemented")
	default:
		return 0, errors.New("this operation is not supported")
	}
}

func main() {
	result, err := calculate2(1, 0, DIVIDE)
	if err != nil {
		fmt.Println("error: ", err.Error())
	} else {
		fmt.Println("result: ", result)
	}

	result, err = calculate2(1, 4, DIVIDE)
	if err != nil {
		fmt.Println("error: ", err.Error())
	} else {
		fmt.Println("result: ", result)
	}

	result, err = calculate2(1, 3, SUBTRACTION)
	if err != nil {
		fmt.Println("error: ", err.Error())
	} else {
		fmt.Println("result: ", result)
	}
}
