package main

import (
	"fmt"
	"strconv"
	"time"
)

func receiver(channel chan int, receiverId int) {
	for true {
		value, status := <-channel
		if status {
			fmt.Println("receiverId: ", receiverId, "data byla uspesne prijata: ", value)
		} else {
			fmt.Println("receiverId: ", receiverId, " kanal je pro prijemce uzavreny")
		}
		time.Sleep(1 * time.Second)
	}
}

func logReceiver(logChannel chan string) {
	for true {
		value, status := <-logChannel
		if status {
			fmt.Println("log data: ", value)
		}
	}
}

func logSender(senderId int, logChannel chan string) {
	logChannel <- "LOG - sender posila data: " + strconv.Itoa(senderId) + "\n"
}

func sender(channel chan int, senderId int, logChannel chan string) {
	fmt.Println("senderId: ", senderId, " byl spusten")
	for i := 0; i < 10; i++ {
		channel <- 1
		time.Sleep(1 * time.Second)

		go logSender(senderId, logChannel)
	}
	fmt.Println("senderId: ", senderId, " byl ukoncen")
}

func readAndWrite() {
	text := ""

	channel1 := make(chan int)
	channel2 := make(chan int)
	channel3 := make(chan int)
	channel4 := make(chan int)

	senderLog := make(chan string)
	receiverLog := make(chan string)

	go receiver(channel1, 1)
	go receiver(channel2, 2)

	go sender(channel2, 1, senderLog)
	go sender(channel4, 2, senderLog)

	go logReceiver(receiverLog)

	for i := 0; i < 10; i++ {
		fmt.Println("\n\n---- begin: ", i, " ----")
		select {
		case channel1 <- 0:
			fmt.Println("posilam 0 do kanalu 1")
		case channel2 <- 1:
			fmt.Println("posilam 1 do kanalu 2")
		case data, status := <-channel3:
			if status {
				fmt.Println("prijal jsem data: ", data, " do kanalu 3")
			}
		case data, status := <-channel4:
			if status {
				fmt.Println("prijal jsem data: ", data, " do kanalu 4")
			}
		case data, status := <-senderLog:
			if status {
				text += data
			}
			//case data, status := <-receiverLog:
			//	if status {
			//		fmt.Println("receiver log, data: ", data)
			//	}
		}
		fmt.Println("---- end: ", i, " ----")
	}

	//data, status := <-senderLog
	//if status {
	//	fmt.Println(data)
	//}
	fmt.Println(text)
}

func main() {
	readAndWrite()
}
