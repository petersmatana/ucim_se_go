package main

import (
	"fmt"
	"time"
)

func worker(ch chan int) {
	ch <- 1
}

func worker2(ch chan int, workerID int) {
	fmt.Printf("worker %d spusten\n", workerID)
	time.Sleep(2 * time.Second)
	ch <- 1
	fmt.Printf("worker %d ukoncen\n", workerID)
}

func spatnaVerze() {
	channel1 := make(chan int)
	channel2 := make(chan int)

	go worker(channel1)
	go worker(channel2)

	code, status := <-channel1
	fmt.Printf("code: %d, status: %t\n", code, status)

	code, status = <-channel2
	fmt.Printf("code: %d, status: %t\n", code, status)
}

func lepsiVerze() {
	channel1 := make(chan int)
	channel2 := make(chan int)

	go worker2(channel1, 1)
	go worker2(channel2, 2)

	for true {
		select {
		case <-channel1:
			fmt.Println("data z kanalu 1")
		case <-channel2:
			fmt.Println("data z kanalu 2")
		default:
			fmt.Println("zadna data nejsou k dispozici")
		}
		time.Sleep(2 * time.Second)
	}
}

func main() {
	//spatnaVerze()
	lepsiVerze()
}
