package main

import "fmt"

func worker3(channel chan int) {
	for true {
		value, status := <-channel
		if status {
			fmt.Println("prijata hodnota: ", value, " | status je: ", status)
		} else {
			fmt.Println("kanal je uzavren. hodnota: ", value, " | status je: ", status)
		}
	}
}

func worker4(channel chan int, workerId int) {
	for true {
		value, status := <-channel
		if status {
			fmt.Println("workerID: ", workerId,
				" | prijata hodnota: ", value, " | status je: ", status)
		} else {
			fmt.Println("workerID: ", workerId,
				" | kanal je uzavren. hodnota: ", value, " | status je: ", status)
		}
	}
}

func begin() {
	channel1 := make(chan int)

	go worker3(channel1)

	for i := 0; i < 10; i++ {
		select {
		case channel1 <- 0:
			fmt.Println("posilam do kanalu 0")
		case channel1 <- 1:
			fmt.Println("posilam do kanalu 1")
		}
	}
}

func blokujiciZapisDoKanalu() {
	channel1 := make(chan int)

	go worker4(channel1, 1)
	go worker4(channel1, 2)
	go worker4(channel1, 3)

	for i := 0; i < 10; i++ {
		fmt.Println("---- begin: ", i, " ----")
		select {
		case channel1 <- 0:
			fmt.Println("posilam do kanalu 0")
		case channel1 <- 1:
			fmt.Println("posilam do kanalu 1")
		case channel1 <- 2:
			fmt.Println("posilam do kanalu 2")
		}
		fmt.Println("---- end: ", i, " ----")
	}
}

func main() {
	//begin()
	blokujiciZapisDoKanalu()
}
