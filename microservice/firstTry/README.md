# rozjet GCP SDK
pod ubuntu
zdroj: https://cloud.google.com/sdk/docs/downloads-apt-get

1. pridat repo
   `echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list & apt update`

2. je treba mit apt-transport-https
   `apt install apt-transport-https ca-certificates gnupg`

3. stahnout klice
   `curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -`

4. install
   `apt install google-cloud-sdk`

## jak poznam ze to (ne)mam nakonfigurovany

nemam nakonfigurovany:
```
$ kubectl config view

apiVersion: v1
clusters: null
contexts: null
current-context: ""
kind: Config
preferences: {}
users: null
```

takze musim dat:
https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-access-for-kubectl#generate_kubeconfig_entry

`gcloud container clusters get-credentials <cluster-name>`

kde cluster name mam z: `gcloud container clusters list`



# prihlaseni do GCP

delam to ex post takze v tomto poradi jsem nejspis sazel prikazy:
1. `gcloud auth login`
2. `gcloud auth configure-docker`

## uzitecne info o meme clusteru

1. znat zone, nemam poneti proc se me na to pta: europe-west3-a
2. znat region, ale nevim kde to mam v GUI zjistit.
3. napr, chvili mi trvalo zjistit, kde najdu project id: coz je v mem pripade trymicroservice 

# konfigurace GCP Container registry

(abych tam mohl pushnout image, musim mit nakonfigurovanou komandlajnu)

`hostname/project-id/image`

```
eu.gcr.io/trymicroservice/uservice
eu.gcr.io/trymicroservice/client
```

pokud jsem korektne prihlaseny CLI do apky gcloud, musim pustit tenhle command:
`gcloud auth configure-docker`

tagovani:
```
docker tag i eu.gcr.io/trymicroservice/uservice
docker tag i eu.gcr.io/trymicroservice/client
```

# kompilace uService

## GoLand

eee ???

## CLI

`go build main.go controllers.go utils.go`

# hello world projekt

zdroj: https://cloud.google.com/kubernetes-engine/docs/quickstarts/deploying-a-language-specific-app#go_1

1. kubectl uz mam nainstalovane
2. inicializoval jsem si golang projekt
3. zkopiroval jsem kod a pridal jsem tam check myho env, uvidime
4. zkopiroval jsem Dockerfile a na 2. line jsem pridal: ENV MOJEPROMENNA="moje", uvidime
5. timhle si zjistim jak se muj projekt presne jmenuje: `gcloud config get-value project`
6. `gcloud builds submit --tag gcr.io/project-id/helloworld-gke .`

## pak to naklikavam v k8n

1. existing container
2. envs..
3. projdu wizard..

## pripojit se do nody

`kubectl exec -it <pod_name> -- sh`

abych se doklikal servisy, musim si nastavit ingress.

# docker uService
jak to cely rozjet
1. `docker build -t uservice .`

2. `docker run -ti uservice 9090 10`

# jak se naconnectit

`kubectl exec -it <pod_name> -- sh`

# setrim naklady

1. vylistuju si vsechny k8n clustery

`gcloud container clusters list`

2. nastavim pocet nodu na 0

`gcloud container clusters resize my-first-cluster-1 --num-nodes=0 --zone=europe-west3-a`

3. kdyz budu chtit neco delat, zvysim pocet nodu. default byl 3

`gcloud container clusters resize my-first-cluster-1 --num-nodes=2 --zone=europe-west3-a`

# GCP PostgreSQL

je potreba si davat pozor pri vytvareni instance na veci typu
databaze nebezi na verejne IPv4 adrese atd :D

## napojeni GCP PG na muj lokalni DataGrip

cerpam z:
https://cloud.google.com/sql/docs/postgres/sql-proxy#linux-64-bit

