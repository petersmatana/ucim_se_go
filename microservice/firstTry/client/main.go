package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
)

type DataToSend struct {
	Array []int `json:"array"`
}

type Configuration struct {
	Port int `yaml:"Port"`
	ServicesPort []int `yaml:"ServicesPort"`
	JobsCount int `yaml:"JobsCount"`
}

func loadConfiguration() (cfg Configuration) {
	file := "/home/smonty/gitlab/ucim_se_go/microservice/firstTry/client/config.yaml"
	f, err := os.Open(file)
	if err != nil {
		fmt.Println("can't open file", file, "error:", err)
	}
	defer f.Close()

	var tmp Configuration
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&tmp)
	if err != nil {
		fmt.Println("error in decoding yaml:", err)
		fmt.Println("quit program")
		os.Exit(1)
	}

	return tmp
}

func main() {
	cfg := loadConfiguration()

	router := mux.NewRouter()
	router.HandleFunc("/sendData", cfg.sendData)

	fmt.Println("\nrunning CLIENT\nclient run on port", strconv.Itoa(cfg.Port))
	fmt.Println("==============")

	log.Fatal(http.ListenAndServe(":" + strconv.Itoa(cfg.Port), router))
}

func (cfg Configuration) sendData(writer http.ResponseWriter, request *http.Request) {
	//endpoint := "acceptDataGO"
	endpoint := "acceptDataSYNC"

	for _, item := range cfg.ServicesPort {
		go callEndpoint(item, cfg.JobsCount, endpoint)
	}
}

func generateArray(size int) []int {
	var myArray []int
	for i := 0; i < size; i++ {
		myArray = append(myArray, rand.Intn(22))
	}

	return myArray
}

func callEndpoint(port int, arrayLength int, endpoint string) {
	url := fmt.Sprintf("http://localhost:%d/%s", port, endpoint)
	fmt.Println("url: ", url)

	tmpData := &DataToSend{Array: generateArray(arrayLength)}
	jsonValue, err := json.Marshal(tmpData)
	if err != nil {
		fmt.Println("Marshal error: ", err)
	}

	response, err := http.Post(url, "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		fmt.Println("fail", err)
	} else {
		response, _ := ioutil.ReadAll(response.Body)
		fmt.Println("request pass, response: ", response)
	}
}
