package main

import (
	"fmt"
	"net/http"
	"sync"
	"time"
)

func (cfg Configuration) acceptDataGO(writer http.ResponseWriter, request *http.Request) {
	start := time.Now()

	startProcessingGO(getDataFromRequest(request), cfg.ChannelSize)

	elapsed := time.Since(start)
	fmt.Println("processing finished, time consumed: ", elapsed)

	writer.WriteHeader(http.StatusOK)
}

func startProcessingGO(data DataToReceive, channelSize int) {
	channel := make(chan ProcessData, channelSize)

	go func() {for _, data := range data.Array {
		go processing(channel, data)
	}}()

	//for {
	//	_, status := <- channel
	//	if status {
	//		//fmt.Printf("processd data, value: %d, process time: %.2f\n", value.inputData, value.processingTime)
	//	} else {
	//		// tohle nevim jestli je dobra technika. proste cekam coz je korektni, ale muzu cekat dlouho?
	//		close(channel)
	//	}
	//}

	//for range channel {
	//	<-channel
	//	fmt.Printf("processd data, value: %d, process time: %.2f\n", value.inputData, value.processingTime)
	//}

	// tohle mi radi Tomas a smrdi mi iterovat nad polem,
	// chci iterovat nad dokoncenyma goroutine. tohle
	// se mi nelibi
	for range data.Array {
		<- channel
		//value := <- channel
		//fmt.Printf("processd data, value: %d, process time: %.2f\n", value.inputData, value.processingTime)
	}

	close(channel)
}

func (cfg Configuration) acceptDataSYNC(writer http.ResponseWriter, request *http.Request) {
	start := time.Now()

	startProcessingSYNC(getDataFromRequest(request))

	elapsed := time.Since(start)
	fmt.Println("processing finished, time consumed: ", elapsed)

	writer.WriteHeader(http.StatusOK)
}

func startProcessingSYNC(data DataToReceive) {
	var waitGroup sync.WaitGroup

	waitGroup.Add(len(data.Array))
	//waitGroup.Add(1)
	for _, data := range data.Array {
		go func() {
			defer waitGroup.Done()
			processingWithoutChannel(data)
		}()
	}
	fmt.Println("end for cycle")
	waitGroup.Wait()
	fmt.Println("wait")
	//waitGroup.Done()
	fmt.Println("done")
}
