package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"gopkg.in/yaml.v2"
	"log"
	"net/http"
	"os"
	"strconv"
)

type Configuration struct {
	Env string `yaml:"Env"`
	Port int `yaml:"Port"`
	ChannelSize int `yaml:"ChannelSize"`
}

func loadConfiguration() (cfg Configuration) {
	file := "/home/smonty/gitlab/ucim_se_go/microservice/firstTry/uService/config.yaml"
	f, err := os.Open(file)
	if err != nil {
		fmt.Println("can't open file", file, "error:", err)
	}
	defer f.Close()

	var tmp Configuration
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&tmp)
	if err != nil {
		fmt.Println("error in decoding yaml:", err)
		fmt.Println("quit program")
		os.Exit(1)
	}

	return tmp
}

func (cfg Configuration) loadPort() (port int) {
	if cfg.Env == "dev" {
		return cfg.Port
	}

	if cfg.Env == "prod" {
		port, err := strconv.Atoi(os.Getenv("PORT"))
		if err != nil {
			return port
		}
	}

	return -1
}

func main() {
	cfg := loadConfiguration()
	port := cfg.loadPort()

	fmt.Printf("port %d, channel size %d, environment %s\n", port, cfg.ChannelSize, cfg.Env)
	fmt.Println("==============")

	router := mux.NewRouter()
	router.HandleFunc("/acceptDataGO", cfg.acceptDataGO)
	router.HandleFunc("/acceptDataSYNC", cfg.acceptDataSYNC)

	log.Fatal(http.ListenAndServe(":" + strconv.Itoa(port), router))
}
