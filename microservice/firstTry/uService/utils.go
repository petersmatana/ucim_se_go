package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"time"
)

type DataToReceive struct {
	Array []int `json:"array"`
}

type ProcessData struct {
	inputData int
	processingTime float64
}

func processing(channel chan ProcessData, value int) {
	start := time.Now()
	time.Sleep(time.Duration(value * getRandomValue()) * time.Microsecond)
	elapsed := time.Since(start)

	channel <- ProcessData{inputData: value, processingTime: float64(elapsed.Milliseconds())}
}

func processingWithoutChannel(value int) ProcessData {
	start := time.Now()
	time.Sleep(time.Duration(value * getRandomValue()) * time.Microsecond)
	elapsed := time.Since(start)

	return ProcessData{inputData: value, processingTime: float64(elapsed.Milliseconds())}
}

func getDataFromRequest(request *http.Request) DataToReceive {
	var data DataToReceive
	err := json.NewDecoder(request.Body).Decode(&data)
	if err != nil {
		fmt.Println("parsing error: ", err)
	}

	return data
}

func getRandomValue() int {
	return rand.Intn(120000-80000) + 75000
}
