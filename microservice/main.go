package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/goodbye", func(writer http.ResponseWriter, request *http.Request) {
		log.Println("/goodbye endpoint")
	})

	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		log.Println("/ endpoint")

		body, err := ioutil.ReadAll(request.Body)
		if err != nil {
			log.Println("Error reading body", err)

			http.Error(writer, "Unable to read request body", http.StatusBadRequest)
			//return
		} else {
			//log.Println("body: ", body)
			fmt.Fprintf(writer, "Hello %s", body)
		}

		// write the response
	})

	log.Println("starting server")
	err := http.ListenAndServe(":9090", nil)
	log.Fatalln(err)
}
