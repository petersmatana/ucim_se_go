package main

import "fmt"

func createBigNumber(c chan int, smallNumber int) {
	c <- smallNumber * 1000
	close(c)
}

func generateNumbers(c chan int) {
	for i := 1; i <= 10; i++ {
		c <- i
	}
	close(c)
}

func main() {
	smallNumbers := make(chan int)
	bigNumbers := make(chan int)

	go generateNumbers(smallNumbers)

	for smallNumber := range smallNumbers {
		fmt.Printf("generated small number: %d\n", smallNumber)

		go createBigNumber(bigNumbers, smallNumber)

		for bigNumber := range bigNumbers {
			fmt.Printf("create big number: %d\n", bigNumber)
		}
	}
}
