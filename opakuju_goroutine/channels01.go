// https://medium.com/rungo/anatomy-of-channels-in-go-concurrency-in-go-1ec336086adb

package main

import "fmt"

func main() {
	// takto si deklaruju channel ale je nil
	// takze z neho nemuzu cist ani zapisovat
	var c chan int
	fmt.Print(c)
}
