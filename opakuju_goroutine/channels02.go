package main

import "fmt"

func main() {
	// takto si vytvorim channel typu int
	c := make(chan int)

	fmt.Printf("type of `c` is %T\n", c)
	fmt.Printf("value of `c` is %v\n", c)
}
