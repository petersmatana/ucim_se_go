package main

/*

first example:
	funguje to tak, ze si vytvorim kanal, pak asynchrone poustim
	gorutinu greet. ta se ale zablokuje protoze se cte z kanalu,
	viz, <- c. no a jak zavolam go greet() tak pak zapisuju do
	kanalu string, blokovana gorutina pokracuje dal v behu

second example:
	problem proc toto nefunguje je ze mam kanal ktery nema velikost.
	ja do nej zapisu a hned potrebuju neco co z neho cte. takze kdyz
	si "v prvnim taktu" vytvorim kanal bez velikosti, v "drume taktu"
	do nej zapisu tak se zablokuju protoze nekdo z neho musi cist ale
	cteni se nestane protoze jsem blokovany. takze se ani nedostanu k
	cteni value, status := <- channel.

*/

import "fmt"

func greet(c chan string) {
	// <- c ~ zde ctu / vypisuju z kanalu c
	// do printovani
	fmt.Println("greet() started")
	fmt.Println("Hello " + <- c + "!")
	fmt.Println("greet() stopped")
}

func firstExample() {
	fmt.Println("main() started")

	c := make(chan string)

	// asynchronne poustim greet fci ale nejsem si presne jistej jak
	// je to s jejim blokovanim.
	// nemelo by to byt tak ze goroutine je blokovana az z ni nekdo cte?
	go greet(c)

	// zapisuju do kanalu string
	c <- "smonty"
	fmt.Println("main() stopped")
}

func secondExample() {
	channel := make(chan int)

	channel <- 10
	//value := safeSend(channel, 10)
	//unsafeSend(channel, 10)

	value, status := <- channel
	if status {
		fmt.Println("value", value)
	}

	close(channel)
}

func main() {
	//firstExample()
	//secondExample()
}
