// https://go101.org/article/channel-closing.html

package main

import (
	"fmt"
)

func message(id int, channel chan int) {
	fmt.Println("message func, id: ", id)

	// zapis hodnoty do kanalu, blokujici cinnost
	channel <- id
}

func exampleA() {
	channel := make(chan int)

	fmt.Println("start")
	go message(1, channel)

	// blokujici cteni z kanalu
	value, status := <- channel
	if status {
		fmt.Println("prectena hodnota ", value)
	}

	fmt.Println("end")
}

func worker(task_channel chan int, worker_done chan bool) {
	fmt.Println("worker start")
	for {
		value, status := <- task_channel
		if status {
			fmt.Println("worker received value ", value)
			//time.Sleep(1000 * time.Millisecond)
		} else {
			fmt.Println("finish task value ", value)
			worker_done <- true
			//close(worker_done)
			return
		}
	}
}

func exampleB() {
	task_channel := make(chan int)
	worker_done := make(chan bool)

	fmt.Println("start example")
	defer fmt.Println("end example")

	go worker(task_channel, worker_done)
	for i := 0; i < 10; i++ {
		task_channel <- i
	}
	close(task_channel)

	//for {
	//	_, status := <- worker_done
	//	if status {
	//		fmt.Println("worker done")
	//	}
	//}

	value, status := <- worker_done
	fmt.Println("in the end: status ", status, " value ", value)
	close(worker_done)
}

func main() {
	//exampleA()
	exampleB()
}
