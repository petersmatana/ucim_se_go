package main

import "fmt"

func example1() {
	fmt.Println("main() started")

	// priklad deadlocku
	c := make(chan string)
	val, ok := <- c
	if ok == true {
		fmt.Println(val)
	}
	//c <- "deadlock"

	fmt.Println("main() stopped")
}

func example2() {
	ch := make(chan int)
	ch <- 1
	fmt.Println(ch)
}

func example2_fix() {
	ch := make(chan int)

	go func() {
		ch <- 1
	}()

	val, ok := <- ch
	if ok {
		fmt.Println(val)
	} else {
		fmt.Println("nic")
	}
}

func main() {
	//example1()
	//example2()
	example2_fix()
}
