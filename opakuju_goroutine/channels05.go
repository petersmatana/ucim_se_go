package main

import "fmt"

func goroutine(c chan int) {
	for i := 0; i <= 10; i++ {
		c <- i
	}
	close(c)
}

func endlessFor() {
	c := make(chan int)

	go goroutine(c)

	for {
		val, ok := <- c
		if ok == true {
			fmt.Printf("value in channel %d, status %t\n", val, ok)
		} else {
			fmt.Printf("value in channel %d, status %t\n", val, ok)
			break
		}
	}
}

func usingRange() {
	c := make(chan int)

	go goroutine(c)

	// range mi zavira kanal
	for val := range c {
		fmt.Printf("value in channel %d\n", val)
	}
}

func main() {
	fmt.Println("main() started")

	endlessFor()
	usingRange()

	fmt.Println("main() stopped")
}
