package main

import "fmt"

func isClosed(ch chan int) bool {
	select {
		case <- ch:
			return true
	default:
	}

	return false
}

func main() {
	channel := make(chan int)

	fmt.Println("is channel closed? ", isClosed(channel))
	if ! isClosed(channel) {
		close(channel)
	}
	fmt.Println("is channel closed? ", isClosed(channel))
}
