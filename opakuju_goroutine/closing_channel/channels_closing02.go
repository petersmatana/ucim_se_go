package main

// nehezke zavirani kanalu

func safeSend(channel chan int, value int) (close bool) {
	defer func() {
		if recover() != nil {
			close = true
		}
	}()

	channel <- value
	return false
}

func unsafeSend(channel chan int, value int) {
	channel <- value
}

func safeClose(channel chan int) (closed bool) {
	defer func() {
		if recover() != nil {
			closed = false
		}
	}()

	close(channel)
	return true
}

func main() {
	//channel := make(chan int)
	//
	//channel <- 10
	////value := safeSend(channel, 10)
	////unsafeSend(channel, 10)
	//
	//value, status := <- channel
	//if status {
	//	fmt.Println("value", value)
	//}
	//
	//close(channel)
	////safeClose(channel)
}
