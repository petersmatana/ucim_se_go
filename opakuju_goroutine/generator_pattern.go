package main

import (
	"fmt"
	"time"
)

// navrhovej vzor, kde se rychle generuji
// data, ktera je nutno rychle zpracovat.
// zpracovani muze byt sekvencni ale je to
// pomale protoze se vzdy ceka, nez se zpracuje
// predchazejich hodnota. proto je vhodne
// dany problem paralelne zpracovat. delku
// zpracovavani budu simulovat pomoci sleep

func processAsync(value int) chan string {
	channel := make(chan string)
	go func() {
		time.Sleep(10000 * time.Microsecond)
		channel <- fmt.Sprintf("hodnota %d zpracovana", value)
		close(channel)
	}()
	return channel
}

func processSync(value int) string {
	time.Sleep(10000 * time.Microsecond)
	return fmt.Sprintf("hodnota %d zpracovana", value)
}

func mySolution(value int) string {
	channel := make(chan string)
	message := fmt.Sprintf("hodnota %d zpracovana", value)

	go func() {
		time.Sleep(10000 * time.Microsecond)
		channel <- message
	}()

	//close(channel)

	for {
		val, status := <- channel
		if status {
			fmt.Println(val)
		} else {
			break
		}
	}

	close(channel)

	return message
}

func main() {
	start := time.Now()
	for i := 0; i < 100; i++ {
		//fmt.Println(<- processAsync(i))
		//fmt.Println(processSync(i))
		go fmt.Println(mySolution(i))
	}
	elapsed := time.Since(start)
	fmt.Printf("time need to process: %v\n", elapsed)
}
